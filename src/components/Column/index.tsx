import React from 'react';

import { Task as TaskInterface } from '../../@types';
import Task from '../Task';
import * as S from './styles';

interface ColumnProps {
    title: string;
    items: TaskInterface[];
    handleChangeTaskStatus: (id: string, statusId: number) => void;
}

export default function Column({ title, items, handleChangeTaskStatus } : ColumnProps) {
  return (
    <S.Container>
      <S.Title>
        {title}
      </S.Title>
      <S.TasksContainer>
        {
                    items.map((task) => (
                      <Task
                        key={task.id}
                        id={task.id}
                        name={task.name}
                        statusId={task.statusId}
                        handleChangeTaskStatus={handleChangeTaskStatus}
                      />
                    ))
                }
      </S.TasksContainer>
    </S.Container>
  );
}
