import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex: 1;
    padding: 15px;
    border: 1px solid rgba(0, 0, 0, 0.25);
    box-sizing: border-box;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 15px;
    height: 65vh;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    overflow-y: auto;
    overflow-x: hidden;

    :not(:last-child):not(:first-child) {
        margin: 0px 15px;
    }

    ::-webkit-scrollbar {
    width: 6px;
    height: 6px;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
    background: rgba(0, 0, 0, 0.2);
  }
`;

export const Title = styled.span`
    font-size: 18px;
    font-weight: bold;
`;

export const TasksContainer = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 25px;
    width: 100%;
`;
