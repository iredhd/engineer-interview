import React, { BaseSyntheticEvent, useCallback, useState } from 'react';

import * as S from './styles';

interface AddTaskProps {
    onSubmit: (taskName: string) => void;
}

export default function AddTask({ onSubmit } : AddTaskProps) {
  const [task, setTask] = useState<string>('');

  const handleTextChange = useCallback(({ target: { value } }: BaseSyntheticEvent) => {
    setTask(value);
  }, []);

  const handleSubmitButtonClick = useCallback(() => {
    onSubmit(task);
    setTask('');
  }, [task, onSubmit]);

  return (
    <S.Container>
      <S.Input
        placeholder="Add Task"
        onChange={handleTextChange}
        value={task}
      />
      <S.Button
        disabled={!task}
        onClick={handleSubmitButtonClick}
      >
        +
      </S.Button>
    </S.Container>
  );
}
