import styled from 'styled-components';
import BaseButton from '../Button';

export const Container = styled.div`
    display: flex;
`;

export const Input = styled.input`
    border: 1px solid rgba(0, 0, 0, 0.50);
    padding: 10px;
    margin-right: 15px;
`;

export const Button = styled(BaseButton)`
    font-size: 24px;
    cursor: pointer;
    padding: 5px 10px;
    border-radius: 5px;

    :disabled {
        cursor: not-allowed;
    }
`;
