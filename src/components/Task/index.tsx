import React, { useCallback } from 'react';
import { BsArrowRight, BsArrowLeft } from 'react-icons/bs';
import { firstStatusId, lastStatusId } from '../../constants/status';

import * as S from './styles';

interface TaskProps {
    id: string;
    name: string;
    handleChangeTaskStatus: (id: string, status: number) => void;
    statusId: number;
}

export default function Task({
  id, name, handleChangeTaskStatus, statusId,
}: TaskProps) {
  const handleNextStatusButtonClick = useCallback(() => {
    handleChangeTaskStatus(id, statusId + 1);
  }, [id, statusId]);

  const handleLastStatusButtonClick = useCallback(() => {
    handleChangeTaskStatus(id, statusId - 1);
  }, [id, statusId]);

  return (
    <S.Container>
      <S.Button
        onClick={handleLastStatusButtonClick}
        disabled={firstStatusId === statusId}
        kind="danger"
      >
        <BsArrowLeft />
      </S.Button>
      <S.TaskName>
        {name}
      </S.TaskName>
      <S.Button
        onClick={handleNextStatusButtonClick}
        disabled={lastStatusId === statusId}
        kind="success"
      >
        <BsArrowRight />
      </S.Button>
    </S.Container>
  );
}
