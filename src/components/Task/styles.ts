import styled from 'styled-components';

import BaseButton from '../Button';

export const Container = styled.div`
    padding: 15px;
    border: 1px solid rgba(0, 0, 0, 0.25);
    box-sizing: border-box;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    display: flex;
    flex: 1;
    justify-content: space-between;
    align-items: center;

    :not(:last-child) {
        margin-bottom: 10px;
    }
`;

export const TaskName = styled.span`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    padding: 0px 10px;
`;

export const Button = styled(BaseButton)`
    font-size: 24px;
    cursor: pointer;
    padding: 5px 10px;
    border-radius: 5px;
`;
