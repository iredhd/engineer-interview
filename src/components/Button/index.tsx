import React, { ReactNode, HTMLProps } from 'react';

import * as S from './styles';

export interface ButtonProps extends HTMLProps<HTMLButtonElement> {
    children: ReactNode;
    kind?: 'primary' | 'success' | 'danger';
}

export default function Button({
  children, onClick, kind, disabled, className,
} : ButtonProps) {
  return (
    <S.Container
      onClick={onClick}
      kind={kind}
      disabled={disabled}
      className={className}
    >
      {children}
    </S.Container>
  );
}

Button.defaultProps = {
  kind: 'primary',
};
