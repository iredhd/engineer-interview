import styled from 'styled-components';
import { ButtonProps } from '.';

export const Container = styled.button<ButtonProps>`
    color: white;
    padding: 10px;
    background-color: ${({ theme, kind }) => theme.palette[kind || 'primary'].main};
    border-color: ${({ theme, kind }) => theme.palette[kind || 'primary'].main};

    :disabled {
        background-color: ${({ theme, kind }) => theme.palette[kind || 'primary'].disabled};
        border-color: ${({ theme, kind }) => theme.palette[kind || 'primary'].disabled};
    }
`;
