import React, { useCallback, useMemo, useState } from 'react';
import { get } from 'lodash';
import { v4 } from 'uuid';

import AddTask from '../AddTask';
import Column from '../Column';
import * as S from './styles';
import { Task, TaskSeparatedByStatus } from '../../@types';
import { statusList } from '../../constants/status';

export function ChallengeComponent() {
  const [tasks, setTasks] = useState<Task[]>([]);

  const handleAddTaskSubmit = useCallback((taskName: string) => {
    setTasks((state) => [
      ...state,
      {
        id: v4(),
        name: taskName,
        statusId: 0,
      },
    ]);
  }, []);

  const tasksSeparatedByStatus = useMemo(() => tasks
    .reduce((previous: TaskSeparatedByStatus, current: Task) => ({
      ...previous,
      [current.statusId]: [
        ...get(previous, current.statusId, []),
        current,
      ],
    }), {}), [tasks]);

  const handleTaskStatusChange = useCallback((id: string, newStatusId: number) => {
    setTasks((state: Task[]) => state.map((task: Task) => (task.id === id ? {
      ...task,
      statusId: newStatusId,
    } : task)));
  }, []);

  return (
    <S.Container>
      <S.ColumnsContainer>
        {
        statusList.map((status) => (
          <Column
            key={status.id}
            title={status.name}
            items={get(tasksSeparatedByStatus, status.id, [])}
            handleChangeTaskStatus={handleTaskStatusChange}
          />
        ))
      }
      </S.ColumnsContainer>
      <S.AddTaskContainer>
        <AddTask onSubmit={handleAddTaskSubmit} />
      </S.AddTaskContainer>
    </S.Container>
  );
}
