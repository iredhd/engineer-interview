import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    padding: 25px;
    flex-direction: column;
`;

export const ColumnsContainer = styled.div`
    display: flex;
`;

export const AddTaskContainer = styled.div`
    margin-top: 15px;
`;
