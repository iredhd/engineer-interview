import { DefaultTheme } from 'styled-components';

export const defaultTheme: DefaultTheme = {
  palette: {
    primary: {
      main: 'rgb(0, 101, 255)',
      disabled: 'rgba(0, 101, 255, 0.2)',
    },
    success: {
      main: 'rgb(30, 141, 31)',
      disabled: 'rgba(30, 141, 31, 0.2)',
    },
    danger: {
      main: 'rgb(199, 11, 12)',
      disabled: 'rgba(199, 11, 12, 0.2)',
    },
  },
};
