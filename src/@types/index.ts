export interface Task {
    id: string;
    name: string;
    statusId: number;
  }

export interface Status {
    name: string
    id: number;
}

export interface TaskSeparatedByStatus {
    [key: number]: Task[]
}
