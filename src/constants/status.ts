import { Status } from '../@types';

export const statusList: Status[] = [{
  name: 'To Do',
  id: 0,
}, {
  name: 'In Progress',
  id: 1,
}, {
  name: 'Done',
  id: 2,
}];

export const firstStatusId = statusList
  .reduce((previous, current) => (previous < current.id ? previous : current.id), 0);

export const lastStatusId = statusList
  .reduce((previous, current) => (previous > current.id ? previous : current.id), 0);
