import 'styled-components';

interface Palette {
  main: string;
  disabled: string;
}

declare module 'styled-components' {
  export interface DefaultTheme {
    palette: {
      primary: Palette;
      success: Palette;
      danger: Palette;
   }
  }
}
